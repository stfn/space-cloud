import ImageList from "./images/imageList"

function App() {
  return (
    <div className="App">
      <h1>Space Cloud</h1>
      <ImageList />
    </div>
  );
}

export default App;
