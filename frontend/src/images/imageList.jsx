import React, { useEffect, useState } from "react";
import axios from "axios";

function Users() {
  const [post, setPost] = useState([]);

  useEffect(() => {
    axios.get("http://localhost:8000").then((data) => {
      console.log(data);
      setPost(data?.data);
    });
  }, []);

  return (
    <div>
      Images
      {post.map((item, i) => {
        const thumbnail_url = "http://localhost:8000/" + item.thumbnail_url
        return (
          <div key={i}>
            <p>{item?.filename}</p>
            <img src={thumbnail_url} />

          </div>
        );
      })}
    </div>
  );
}

export default Users;
