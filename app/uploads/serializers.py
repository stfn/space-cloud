from rest_framework import serializers
from uploads.models import UploadTask


class DetailUploadTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = UploadTask
        fields = "__all__"


class ListUploadTaskSerializer(serializers.ModelSerializer):
    thumbnail_url = serializers.SerializerMethodField()

    class Meta:
        model = UploadTask
        fields = [
            "pk",
            "filename",
            "local_destination",
            "timestamp",
            "status",
            "thumbnail_url",
        ]

    def get_thumbnail_url(self, obj):
        fname = obj.filename.replace("fits", "jpg")
        return f"{obj.local_destination}/{fname}"


class UploadTaskInitializeSerializer(serializers.Serializer):
    destination = serializers.CharField()
    file = serializers.FileField()


class EditFitsHeaderSerializer(serializers.Serializer):

    header = serializers.DictField()
